import React, { Component } from 'react';
import { StatusBar } from 'react-native';
import { MenuProvider } from 'react-native-popup-menu';
import { COLORS } from './configs/constants.config';
import StackNavigation from './Navigations/StackNavigation';

export default class App extends Component {
  render() {
    // const isDarkMode = useColorScheme() === 'dark';

    // const backgroundStyle = {
    //   backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
    // };
    return (
      <>
        <MenuProvider>
          <StatusBar
            animated={true}
            backgroundColor={COLORS.primary}
            barStyle={'default'}
            hidden={false} />
          <StackNavigation {...this.props} />
        </MenuProvider>
      </>
    );
  }
}
