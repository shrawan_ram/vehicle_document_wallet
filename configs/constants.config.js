import { Dimensions } from "react-native"

export const AppInfo = {
    name: 'Vehicle Document Wallet',
    version: '1.0.0'
}

export const COLORS = {
    primary: '#2596be',
    white: '#ffffff',
    primary_fade : '#2596be55',
}

export const SCREEN = {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height
}

export const API_BASE = 'https://iagency.in/api/rtoapp/'