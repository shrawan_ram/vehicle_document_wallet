import ApiExecute from ".";
import searchApiExecute from "./searchApi";

export const commanApi = async (url) => {
    let response = await ApiExecute(url);
    return response;
}

export const searchApi = async(data) => {
    
    let response = await searchApiExecute("vehicle_info", { method: 'POST', data:data });
    return response;
}

export const editProfile = async(data) => {
    let response = await ApiExecute("editProfile", { method: 'POST', data:data });
    return response;
}

export const addVehicle = async(data) => {
    let response = await ApiExecute("add_vehicle", { method: 'POST', data:data });
    return response;
}

export const addLicense = async(data) => {
    let response = await ApiExecute("add_license", { method: 'POST', data:data });
    return response;
}

export const addFeedback = async(data) => {
    let response = await ApiExecute("add_feedback", { method: 'POST', data:data });
    return response;
}

export const sendOtp = async (url) => {
    let response = await ApiExecute(url, { method: 'POST' });
    return response;
}
export const login = async (url) => {
    let response = await ApiExecute(url, { method: 'POST' });
    return response;
}
