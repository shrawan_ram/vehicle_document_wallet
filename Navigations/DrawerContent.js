import AsyncStorage from "@react-native-async-storage/async-storage";
import { DrawerContentScrollView } from "@react-navigation/drawer";
import React, { Component } from "react";
import { Image, Linking, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { COLORS } from "../configs/constants.config";
import { commanApi } from "../Api/api";

export default class DrawerContent extends Component {
    state = {
        user: null,
    }
    componentDidMount = async () => {
        let user = await AsyncStorage.getItem('@user');
        if (user) {
            user = JSON.parse(user);
            this.setState({ user: user });
        }
    }
    navigateTo = (path, params = null) => {
        let { navigation } = this.props;

        navigation.navigate(path, params);
    }
    logOut = async () => {
        await AsyncStorage.setItem('@user', '');
        this.goTo('Login');
    }

    logOut = async () => {
        let { user } = this.state;
        let response = await commanApi(`logout?mobile=${user.mobile}`);
        if (response.status) {
            if (response.data.status) {
                await AsyncStorage.setItem('@user', '');
                this.goTo('Login');
            } else {
                Alert.alert(
                    "Error",
                    response.data.message,
                    [
                        { text: "OK", onPress: () => console.log("OK Pressed") }
                    ]
                );
            }
        }
    }
    goTo = path => {
        let { navigation } = this.props;
        navigation.navigate(path);
    }
    render() {
        let user = this.state.user;
        return (
            <>
                <View style={{ backgroundColor: '#f2f2f2', padding: 30 }}>
                    <Image
                        source={require('../assets/logo.png')}
                        // source={{ uri: "https://cdn-icons-png.flaticon.com/128/747/747376.png" }}
                        resizeMode="contain"
                        style={{ width: '100%', height: 100, }}
                    />
                    {/* <Text style={{ fontSize: 18, textAlign: 'center', color: '#fff', padding: 10 }}>Jhon Due</Text> */}
                </View>
                <DrawerContentScrollView {...this.props}>
                    {
                        this.state.user ?
                            <>
                                <TouchableOpacity style={style.list} onPress={() => this.navigateTo('Profile')}>
                                    <Text style={style.navList}>Profile</Text>
                                </TouchableOpacity>
                            </>
                            :
                            <>
                                <TouchableOpacity style={style.list} onPress={() => this.navigateTo('Login1')}>
                                    <Text style={style.navList}>Login</Text>
                                </TouchableOpacity>
                            </>
                    }
                    <TouchableOpacity style={style.list} onPress={() => Linking.openURL('https://play.google.com/store/apps/details?id=com.vehiclewallet.iagency')}>
                        <Text style={style.navList}>Rate this app</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={style.list} onPress={() => Linking.openURL('https://play.google.com/store/apps/details?id=com.vehiclewallet.iagency')}>
                        <Text style={style.navList}>Share with friends</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={style.list} onPress={() => this.navigateTo('PrivancyPolicy')}>
                        <Text style={style.navList}>Privacy policy</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={style.list} onPress={() => this.navigateTo('TermCondition')}>
                        <Text style={style.navList}>Term & condition</Text>
                    </TouchableOpacity>
                    {
                        this.state.user ?
                            <TouchableOpacity style={style.list} onPress={() => this.navigateTo('Feedback')}>
                                <Text style={style.navList}>Feedback</Text>
                            </TouchableOpacity>
                            : <></>
                    }
                    <TouchableOpacity style={style.list} onPress={() => this.navigateTo('OtherApps')}>
                        <Text style={style.navList}>Other apps</Text>
                    </TouchableOpacity>
                    {
                        this.state.user ?
                            <TouchableOpacity style={style.list} onPress={() => this.logOut()}>
                                <Text style={style.navList}>Logout</Text>
                            </TouchableOpacity>
                            : <></>
                    }
                </DrawerContentScrollView>
            </>
        )

    }
}

const style = StyleSheet.create({
    navList: {
        paddingHorizontal: 15,
        paddingVertical: 10,
        fontSize: 16,
    },
    list: {
        borderBottomWidth: 0.5,
        borderColor: COLORS.primary,
    }
});