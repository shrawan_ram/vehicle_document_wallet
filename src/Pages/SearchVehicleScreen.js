import AsyncStorage from "@react-native-async-storage/async-storage";
import moment from "moment";
import React, { Component } from "react";
import { ScrollView, Text, TouchableOpacity, View } from "react-native";
import { List } from "react-native-paper";
import { COLORS } from "../../configs/constants.config";

export default class SearchVehicleScreen extends Component {
    state = {
        expanded1: true,
        expanded2: false,
        expanded3: false,
        expanded4: false,
        vehicleDetail : '',
        user    : ''
    }
    handlePress = (val) => {
        if (val == 1) {
            this.setState({ expanded1: !this.state.expanded1 });
        }
        if (val == 2) {
            this.setState({ expanded2: !this.state.expanded2 });
        }
        if (val == 3) {
            this.setState({ expanded3: !this.state.expanded3 });
        }
        if (val == 4) {
            this.setState({ expanded4: !this.state.expanded4 });
        }
    }
    saveVehicle = async () => {
        let { vehicleDetail, user } = this.state;
        let category = '';
        // if(vehicleDetail.class == 'M-CYCLE/SCOOTER(2WN)'){
        //     category = 'Two Wheeler';
        // }
        // if(vehicleDetail.class == 'M-CYCLE/SCOOTER(2WN)'){
        //     category = 'Two Wheeler';
        // }
        // if(vehicleDetail.class == 'M-CYCLE/SCOOTER(2WN)'){
        //     category = 'Two Wheeler';
        // }
        // if(vehicleDetail.class == 'M-CYCLE/SCOOTER(2WN)'){
        //     category = 'Two Wheeler';
        // }
        // if(vehicleDetail.class == 'M-CYCLE/SCOOTER(2WN)'){
        //     category = 'Two Wheeler';
        // }
        // if(vehicleDetail.class == 'M-CYCLE/SCOOTER(2WN)'){
        //     category = 'Two Wheeler';
        // }

        this.setState({ loading: true });
        
        
        let data = {
            reg_number: vehicleDetail.license_plate,
            category_id: 1,
            user_id: user.id,
        }

        let response = await addVehicle(data);
        console.log('data', response);
        if (response.status) {
            if (response.data.status) {
                setTimeout(() => {
                    this.navigateTo('Vehicle', response.data.data);
                }, 30);
            } else {
                this.setState({ loading: false });
                Alert.alert(
                    "Error",
                    response.data.response_message,
                    [
                        { text: "OK", onPress: () => console.log("OK Pressed") }
                    ]
                );
            }
        }
    }
    async componentDidMount(){
        let {route} = this.props;
        
        this.setState({vehicleDetail: route.params});

        let user = await AsyncStorage.getItem('@user');
        if (user) {
            user = JSON.parse(user);
            console.log('user details', user);
            this.setState({ user: user });
        }
    }
    render() {
        var vehicleDetail = this.state.vehicleDetail;
        console.log('route',vehicleDetail);
        return (
            <>
            <ScrollView>
                <View style={{}}>
                    <View style={{ padding: 15, backgroundColor: '#fff' }}>
                        <Text style={{ textAlign: 'center', fontSize: 19 }}>{vehicleDetail.license_plate}</Text>
                    </View>
                    <View style={{ margin: 15 }}>
                        <List.Section style={{
                            marginTop: 0,
                            backgroundColor: COLORS.white, margin: 15, marginBottom: 0, shadowColor: '#000',
                            shadowOffset: { width: 0, height: 1 },
                            shadowOpacity: 0.8,
                            shadowRadius: 2,
                            elevation: 5,
                            zIndex: 999,
                            padding: 2,
                            borderRadius: 8,
                        }}>
                            <List.Accordion
                                title="Ownership details"
                                titleStyle={{ color: this.state.expanded1 ? COLORS.primary : '#757575' }}
                                style={{ backgroundColor: '#fff' }}
                                expanded={this.state.expanded1}
                                onPress={() => this.handlePress(1)}
                            >
                                <View style={{ paddingHorizontal: 17 }}>
                                    <View style={{ borderBottomWidth: 1, paddingVertical: 5, marginBottom: 10, borderColor: '#ccc' }}>
                                        <Text style={{ fontSize: 12 }}>Owner Name</Text>
                                        <Text style={{ color: '#000' }} selectable>{vehicleDetail.owner_name}</Text>
                                    </View>
                                    {/* <View style={{ borderBottomWidth: 1, paddingVertical: 5, marginBottom: 10, borderColor: '#ccc' }}>
                                        <Text style={{ fontSize: 12 }}>Ownership</Text>
                                        <Text style={{ color: '#000' }}>First Owner</Text>
                                    </View> */}
                                    <View style={{ borderBottomWidth: 1, paddingVertical: 5, marginBottom: 10, borderColor: '#ccc' }}>
                                        <Text style={{ fontSize: 12 }}>Financer`s Name (HP)</Text>
                                        <Text style={{ color: '#000' }} selectable>{vehicleDetail.financer}</Text>
                                    </View>
                                    <View style={{ borderBottomWidth: 1, paddingVertical: 5, marginBottom: 10, borderColor: '#ccc' }}>
                                        <Text style={{ fontSize: 12 }} >Registered RTO</Text>
                                        <Text style={{ color: '#000' }} selectable>{vehicleDetail.rto}</Text>
                                    </View>
                                </View>

                            </List.Accordion>
                            <List.Accordion
                                title="Vehicle details"
                                titleStyle={{ color: this.state.expanded2 ? COLORS.primary : '#757575' }}
                                style={{ backgroundColor: '#fff' }}
                                expanded={this.state.expanded2}
                                onPress={() => this.handlePress(2)}
                            >
                                <View style={{ paddingHorizontal: 17 }}>
                                    <View style={{ borderBottomWidth: 1, paddingVertical: 5, marginBottom: 10, borderColor: '#ccc' }}>
                                        <Text style={{ fontSize: 12 }}>Maker Model</Text>
                                        <Text style={{ color: '#000' }} selectable>{vehicleDetail.model}</Text>
                                    </View>
                                    <View style={{ borderBottomWidth: 1, paddingVertical: 5, marginBottom: 10, borderColor: '#ccc' }}>
                                        <Text style={{ fontSize: 12 }}>Vehicle Class</Text>
                                        <Text style={{ color: '#000' }} selectable>{vehicleDetail.class}</Text>
                                    </View>
                                    <View style={{ borderBottomWidth: 1, paddingVertical: 5, marginBottom: 10, borderColor: '#ccc' }}>
                                        <Text style={{ fontSize: 12 }}>Fule Type</Text>
                                        <Text style={{ color: '#000' }} selectable>{vehicleDetail.fuel_type}</Text>
                                    </View>
                                    {/* <View style={{ borderBottomWidth: 1, paddingVertical: 5, marginBottom: 10, borderColor: '#ccc' }}>
                                        <Text style={{ fontSize: 12 }}>Fule Norms</Text>
                                        <Text style={{ color: '#000' }}>BHARAT STAGE IV</Text>
                                    </View> */}
                                    <View style={{ borderBottomWidth: 1, paddingVertical: 5, marginBottom: 10, borderColor: '#ccc' }}>
                                        <Text style={{ fontSize: 12 }}>Engine No.</Text>
                                        <Text style={{ color: '#000' }} selectable>{vehicleDetail.engine_number}</Text>
                                    </View>
                                    <View style={{ borderBottomWidth: 1, paddingVertical: 5, marginBottom: 10, borderColor: '#ccc' }}>
                                        <Text style={{ fontSize: 12 }}>Chassis No.</Text>
                                        <Text style={{ color: '#000' }} selectable>{vehicleDetail.chassis_number}</Text>
                                    </View>
                                </View>
                            </List.Accordion>
                            <List.Accordion
                                title="Important dates"
                                titleStyle={{ color: this.state.expanded3 ? COLORS.primary : '#757575' }}
                                style={{ backgroundColor: '#fff' }}
                                expanded={this.state.expanded3}
                                onPress={() => this.handlePress(3)}
                            >
                                <View style={{ paddingHorizontal: 17 }}>
                                    <View style={{ borderBottomWidth: 1, paddingVertical: 5, marginBottom: 10, borderColor: '#ccc' }}>
                                        <Text style={{ fontSize: 12 }}>Registration Date</Text>
                                        <Text style={{ color: '#000' }} selectable>{vehicleDetail.registration_date}</Text>
                                    </View>
                                    <View style={{ borderBottomWidth: 1, paddingVertical: 5, marginBottom: 10, borderColor: '#ccc' }}>
                                        <Text style={{ fontSize: 12 }}>Vehicle Age</Text>
                                        <Text style={{ color: '#000' }} selectable>{vehicleDetail.vehicle_age}</Text>
                                    </View>
                                    <View style={{ borderBottomWidth: 1, paddingVertical: 5, marginBottom: 10, borderColor: '#ccc' }}>
                                        <Text style={{ fontSize: 12 }}>Fitness Upto</Text>
                                        <Text style={{ color: '#000' }} selectable>{vehicleDetail.fitness_upto}</Text>
                                    </View>
                                    <View style={{ borderBottomWidth: 1, paddingVertical: 5, marginBottom: 10, borderColor: '#ccc' }}>
                                        <Text style={{ fontSize: 12 }}>Pollution Upto</Text>
                                        <Text style={{ color: '#000' }} selectable>{vehicleDetail.pollution_expiry}</Text>
                                    </View>
                                    <View style={{ borderBottomWidth: 1, paddingVertical: 5, marginBottom: 10, borderColor: '#ccc' }}>
                                        <Text style={{ fontSize: 12 }}>Pollution Expiring In</Text>
                                        <Text style={{ color: '#000' }} selectable>{moment.utc(vehicleDetail.pollution_expiry).local().startOf('seconds').fromNow()}</Text>
                                    </View>
                                    <View style={{ borderBottomWidth: 1, paddingVertical: 5, marginBottom: 10, borderColor: '#ccc' }}>
                                        <Text style={{ fontSize: 12 }}>Insurance Expiry (tentative)</Text>
                                        <Text style={{ color: '#000' }} selectable>{vehicleDetail.insurance_expiry}</Text>
                                    </View>
                                    <View style={{ borderBottomWidth: 1, paddingVertical: 5, marginBottom: 10, borderColor: '#ccc' }}>
                                        <Text style={{ fontSize: 12 }}>Insurance Expiring In</Text>
                                        <Text style={{ color: '#000' }} selectable>{moment.utc(vehicleDetail.insurance_expiry).local().startOf('seconds').fromNow()}</Text>
                                    </View>
                                </View>
                            </List.Accordion>
                            <List.Accordion
                                title="Other info"
                                titleStyle={{ color: this.state.expanded4 ? COLORS.primary : '#757575' }}
                                style={{ backgroundColor: '#fff' }}
                                expanded={this.state.expanded4}
                                onPress={() => this.handlePress(4)}
                            >
                                <View style={{ paddingHorizontal: 17 }}>
                                    <View style={{ borderBottomWidth: 1, paddingVertical: 5, marginBottom: 10, borderColor: '#ccc' }}>
                                        <Text style={{ fontSize: 12 }}>RTO Code</Text>
                                        <Text style={{ color: '#000' }}>{vehicleDetail.rto_code}</Text>
                                    </View>
                                    <View style={{ borderBottomWidth: 1, paddingVertical: 5, marginBottom: 10, borderColor: '#ccc' }}>
                                        <Text style={{ fontSize: 12 }}>RTO</Text>
                                        <Text style={{ color: '#000' }}>{vehicleDetail.rto}</Text>
                                    </View>
                                    <View style={{ borderBottomWidth: 1, paddingVertical: 5, marginBottom: 10, borderColor: '#ccc' }}>
                                        <Text style={{ fontSize: 12 }}>State</Text>
                                        <Text style={{ color: '#000' }}>{vehicleDetail.state}</Text>
                                    </View>
                                    
                                </View>
                            </List.Accordion>
                        </List.Section>
                    </View>
                </View>
            </ScrollView>
            {/* <View onPress={() => this.saveVehicle()}  style={{ marginTop: 10, backgroundColor: COLORS.primary, height: 40, fontSize: 15, alignSelf: 'center', borderRadius: 4, width: '100%', paddingHorizontal: 20, marginTop: 30, justifyContent: 'center',position:'absolute',left:0,right:0,bottom:0 }}>
                <Text style={{ textAlign: 'center', fontSize: 19, color: COLORS.white, fontWeight: 'bold' }}>Set Reminder</Text>
            </View> */}
            </>
        );
    }
}