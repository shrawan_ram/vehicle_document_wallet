import AsyncStorage from "@react-native-async-storage/async-storage";
import React, { Component } from "react";
import { ActivityIndicator, Alert, ScrollView, Text, TextInput, ToastAndroid, TouchableOpacity, View } from "react-native";
import { addFeedback } from "../../Api/api";
import { COLORS } from "../../configs/constants.config";

export default class FeedbackScreen extends Component {
    state = {
        message : '',
        user    : ''
    }
    onChangeText = (text) => {
        this.setState({ message: text });
    }
    saveFeedback = async () => {
        let { message, user } = this.state;

        this.setState({ loading: true });
        let data = {
            description : message,
            user_id     : user.id
        }

        let response = await addFeedback(data);
        if (response.status) {
            
            setTimeout(() => {
                this.setState({ loading: false });
                this.setState({message:''});
                ToastAndroid.show("Your Feedback Send Successfully !", ToastAndroid.SHORT);
            }, 30);

            // Alert.alert(
            //     "Error",
            //     response.data.response_message,
            //     [
            //         { text: "OK", onPress: () => console.log("OK Pressed") }
            //     ]
            // );
            
        }
    }
    async componentDidMount() {
        let user = await AsyncStorage.getItem('@user');
        if (user) {
            user = JSON.parse(user);
            console.log('user details', user);
            this.setState({ user: user });
        }
        
    }
    render() {
        return (
            <ScrollView>
                <View style={{ margin: 15, marginRight: 15, marginBottom: 15, }}>
                    <TextInput
                        multiline
                        numberOfLines={6}
                        onChangeText={text => this.onChangeText(text)}
                        value={this.state.message}
                        placeholder="Message"
                        style={{ padding: 10, borderWidth: 1, borderRadius: 4, borderColor: '#ccc', textAlignVertical: 'top' }}
                    />
                    {
                        this.state.message 
                        ?
                            this.state.loading 
                            ?

                            <TouchableOpacity style={{ marginTop: 10, backgroundColor: COLORS.primary, height: 40, fontSize: 15, alignSelf: 'center', borderRadius: 4, width: '100%', paddingHorizontal: 20, marginTop: 30, justifyContent: 'center' }}>
                                <ActivityIndicator size="small" color={COLORS.white} />
                            </TouchableOpacity>
                            :
                            <TouchableOpacity onPress={() => this.saveFeedback()}  style={{ marginTop: 10, backgroundColor: COLORS.primary, height: 40, fontSize: 15, alignSelf: 'center', borderRadius: 4, width: '100%', paddingHorizontal: 20, marginTop: 30, justifyContent: 'center' }}>
                                <Text style={{ textAlign: 'center', fontSize: 19, color: COLORS.white, fontWeight: 'bold' }}>SUBMIT</Text>
                            </TouchableOpacity>
                        :
                        <View style={{ marginTop: 10, backgroundColor: COLORS.primary_fade, height: 40, fontSize: 15, alignSelf: 'center', borderRadius: 4, width: '100%', paddingHorizontal: 20, marginTop: 30, justifyContent: 'center' }}>
                            <Text style={{ textAlign: 'center', fontSize: 19, color: COLORS.white, fontWeight: 'bold' }}>SUBMIT</Text>
                        </View>

                    }
                </View>
            </ScrollView>
        );
    }
}