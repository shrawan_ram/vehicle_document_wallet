import React, { Component } from "react";
import { ScrollView, Text, View } from "react-native";

export default class NotificationScreen extends Component {
    render() {
        return (
            <ScrollView>
                <View style={{}}>
                    <View style={{ padding: 15, borderBottomWidth: 1, borderColor: '#ccc' }}>
                        <Text style={{ fontSize: 15 }}>This is first notification.</Text>
                        <Text style={{ fontSize: 10 }}>2 min ago</Text>
                    </View>
                    <View style={{ padding: 15, borderBottomWidth: 1, borderColor: '#ccc' }}>
                        <Text style={{ fontSize: 15 }}>This is first notification.</Text>
                        <Text style={{ fontSize: 10 }}>2 min ago</Text>
                    </View>
                    <View style={{ padding: 15, borderBottomWidth: 1, borderColor: '#ccc' }}>
                        <Text style={{ fontSize: 15 }}>This is first notification.</Text>
                        <Text style={{ fontSize: 10 }}>2 min ago</Text>
                    </View>
                </View>
            </ScrollView>
        );
    }
}