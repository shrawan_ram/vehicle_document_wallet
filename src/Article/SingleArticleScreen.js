import React, { Component } from "react";
import { ActivityIndicator, Image, ScrollView, Text, View } from "react-native";
import { COLORS } from "../../configs/constants.config";

export default class SingleArticleScreen extends Component {
    state = {
        loading: false
    }
    render() {
        let article = this.props.route.params;
        return (
            <>
                {
                    this.state.loading ?
                        <ActivityIndicator size="large" color={COLORS.primary} style={{ marginTop: 30 }} />
                        :
                        <ScrollView>
                            <View style={{ paddingHorizontal: 10, }}>
                                <View style={{ justifyContent: 'center', paddingTop: 10, paddingBottom: 10 }}>
                                    <Image
                                        source={{ uri: article.image_url }}
                                        resizeMode="contain"
                                        style={{
                                            width: '100%',
                                            height: 150
                                        }} />
                                </View>
                                <View style={{ paddingRight: 10, paddingLeft: 10, paddingBottom: 10 }}>
                                    <Text style={{ fontWeight: 'bold', fontSize: 17 }}>{article.name}</Text>
                                    <Text style={{ fontSize: 15, textAlign: 'justify' }}>{article.description}</Text>
                                </View>
                            </View>
                        </ScrollView>
                }
            </>
        );
    }
}