import moment from "moment";
import React, { Component } from "react";
import { ActivityIndicator, Alert, Image, ScrollView, Text, View } from "react-native";
import { commanApi } from "../../Api/api";
import { COLORS } from "../../configs/constants.config";

export default class SingleVehicleScreen extends Component {
    state = {
        loading: false,
        vehicle: ''
    }
    async componentDidMount(){
        let {route} = this.props;
        let url = `vehicle_detail/`+route.params.id;
        this.setState({ loading: true });
        let response = await commanApi(url);
        console.log('res', response);
        if (response.status) {
            if (response.data.status) {
                this.setState({ vehicle: response.data.data });
                setTimeout(() => {
                    this.setState({ loading: false });
                }, 30);
            } else {
                this.setState({ loading: false });
                Alert.alert(
                    "Error",
                    response.data.message,
                    [
                        { text: "OK", onPress: () => console.log("OK Pressed") }
                    ]
                );
            }
        }
    }
    render() {
        let vehicle = this.state.vehicle;
        console.log(vehicle);
        return (
            <>
                {
                    this.state.loading ?
                        <ActivityIndicator size="large" color={COLORS.primary} style={{ marginTop: 30 }} />
                        :
                        <ScrollView>
                            <View style={{ paddingHorizontal: 20, paddingTop: 10, paddingBottom: 10 }}>
                                {/* <View style={{ justifyContent: 'center', paddingTop: 10, paddingBottom: 10 }}>
                                    <Image
                                        source={{ uri: vehicle.image_url }}
                                        resizeMode="contain"
                                        style={{
                                            width: '100%',
                                            height: 150
                                        }} />
                                </View> */}
                                <View style={{ flexDirection: 'row', paddingVertical: 10 }}>
                                    <Text style={{ flex: 5 }}>Vehicle Number :</Text>
                                    <Text style={{ flex: 5, textTransform: 'uppercase' }}>{vehicle.reg_number}</Text>
                                </View>
                                <View style={{ flexDirection: 'row', paddingBottom: 10 }}>
                                    <Text style={{ flex: 5 }}>Category :</Text>
                                    <Text style={{ flex: 5, textTransform: 'uppercase' }}>{vehicle.category ? vehicle.category.name : ''}</Text>
                                </View>
                                {
                                    vehicle.vehicle_documents && vehicle.vehicle_documents.length != 0 
                                    ?
                                    <Text style={{ fontSize: 17, fontWeight: 'bold', textAlign: 'center', paddingBottom: 10 }}>Documents</Text>
                                    :
                                    <></>
                                }

                                {
                                    vehicle.vehicle_documents && vehicle.vehicle_documents.map((vd, index) => {
                                        return(
                                            <>
                                            <Text style={{ fontWeight: 'bold', }}>{vd.document ? vd.document.name : ''}</Text>
                                            <View style={{ flexDirection: 'row', paddingVertical: 10 }}>
                                                <Text style={{ flex: 5 }}>Expiry Date :</Text>
                                                <Text style={{ flex: 5 }}>{moment(vd.expiry_date).format('DD MMM YYYY')}</Text>
                                            </View>
                                            {(() => {
                                                if (vd.image != '') {
                                                    return (
                                                        <>
                                                            <View>
                                                                <Text>File : </Text>
                                                            </View>
                                                            <View style={{ borderWidth: .8, borderColor: '#ccc', marginVertical: 10 }}>
                                                                <Image source={{uri: vd.image_url}} style={{height:150,resizeMode:'center'}} />
                                                            </View>
                                                        </>
                                                    )
                                                }
                                                return null;
                                            })()}
                                            
                                            </>
                                        )
                                    })
                                }

                            </View>
                        </ScrollView>
                }
            </>
        );
    }
}