import AsyncStorage from "@react-native-async-storage/async-storage";
import moment from "moment";
import React, { Component } from "react";
import { ActivityIndicator, Pressable, ScrollView, Text, TouchableOpacity, View } from "react-native";
import { Icon } from "react-native-elements";
import { commanApi } from "../../Api/api";
import { COLORS } from "../../configs/constants.config";

export default class ExpiryVehicleScreen extends Component {
    state = {
        loading: false,
        user: null,
        language: 'en',
        vehicles: []
    }

    navigateTo = (path, params = null) => {
        let { navigation } = this.props;

        navigation.navigate(path, params);
    }


    ArticleApi = async () => {
        let { language, user } = this.state;
        let url = `vehicles?language=${language}&user_id=${user.id}`
        this.setState({ loading: true });
        let response = await commanApi(url);
        console.log('res', response);
        if (response.status) {
            if (response.data.status) {
                this.setState({ vehicles: response.data.data });
                setTimeout(() => {
                    this.setState({ loading: false });
                }, 30);
            } else {
                this.setState({ loading: false });
                Alert.alert(
                    "Error",
                    response.data.message,
                    [
                        { text: "OK", onPress: () => console.log("OK Pressed") }
                    ]
                );
            }
        }
    }
    async componentDidMount() {
        let user = await AsyncStorage.getItem('@user');
        if (user) {
            user = JSON.parse(user);
            console.log('user details', user);
            this.setState({ user: user });
        }
        this.ArticleApi();
    }

    render() {
        let {route} = this.props;
        let vehicle = route.params;
        console.log('vehicle', vehicle);
        
        return (
            <>
                {
                    this.state.loading ?
                        <ActivityIndicator size="large" color={COLORS.primary} style={{ marginTop: 30 }} />
                        :
                        <>
                            <ScrollView>
                                <View style={{ marginLeft: 15, marginRight: 15, marginBottom: 15 }}>
                                    <View>
                                        <Text style={{ marginTop: 10, color: COLORS.primary, fontWeight: 'bold', fontSize: 16 }}>{vehicle.name}</Text>
                                        {
                                            vehicle.vehicle_document.map((item, index) => {
                                                return (
                                                    
                                                    <View style={{ marginTop: 15, backgroundColor: COLORS.white, shadowColor: '#000', shadowOffset: { width: 0, height: 1 }, shadowOpacity: 0.8, shadowRadius: 2, elevation: 5, borderRadius: 8, borderRightWidth: 5, borderRightColor: item.expired ? 'red' : item.expiring ? 'orange' : 'green' }}>
                                                        <TouchableOpacity style={{ flexDirection: 'row' }} onPress={() => this.navigateTo('SingleVehicle', item.vehicle)}>
                                                            <View style={{ flex: 0.2, justifyContent: 'center', paddingTop: 10, paddingBottom: 10 }}>
                                                                {(() => {
                                                                    if (item.vehicle.category_id == 1) {
                                                                        return (
                                                                            <Icon
                                                                                name='car'
                                                                                type='antdesign'
                                                                                color='#000'
                                                                                size={32}
                                                                            />
                                                                        )
                                                                    }
                                                                    return null;
                                                                })()}
                                                                {(() => {
                                                                    if (item.vehicle.category_id == 2) {
                                                                        return (
                                                                            <Icon
                                                                                name='motorcycle'
                                                                                type='materialicon'
                                                                                color='#000'
                                                                                size={32}
                                                                            />
                                                                        )
                                                                    }
                                                                    return null;
                                                                })()}
                                                                {(() => {
                                                                    if (item.vehicle.category_id == 3) {
                                                                        return (
                                                                            <Icon
                                                                                name='truck'
                                                                                type='feather'
                                                                                color='#000'
                                                                                size={32}
                                                                            />
                                                                        )
                                                                    }
                                                                    return null;
                                                                })()}
                                                                {(() => {
                                                                    if (item.vehicle.category_id == 4) {
                                                                        return (
                                                                            <Icon
                                                                                name='bus-outline'
                                                                                type='ionicon'
                                                                                color='#000'
                                                                                size={32}
                                                                            />
                                                                        )
                                                                    }
                                                                    return null;
                                                                })()}
                                                                {(() => {
                                                                    if (item.vehicle.category_id == 5) {
                                                                        return (
                                                                            <Icon
                                                                                name='car'
                                                                                type='antdesign'
                                                                                color='#000'
                                                                                size={32}
                                                                            />
                                                                        )
                                                                    }
                                                                    return null;
                                                                })()}

                                                            </View>
                                                            <View style={{ flex: 0.8, paddingTop: 10, paddingBottom: 10 }}>
                                                                <Text style={{ textTransform: 'uppercase' }}>{item.vehicle.reg_number}</Text>
                                                                <Text style={{ fontSize: 11 }}>Added {moment.utc(item.vehicle.created_at).local().startOf('seconds').fromNow()}</Text>
                                                            </View>
                                                        </TouchableOpacity>
                                                    </View>
                                                           
                                                )
                                            })
                                        }
                                    </View>
                                </View>
                            </ScrollView>
                        </>
                }
            </>
        );
    }
}