import { Picker } from "@react-native-picker/picker";
import moment from "moment";
import React, { Component } from "react";
import { Alert, Modal, Pressable, ScrollView, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import DatePicker from "react-native-date-picker";
import { Icon } from "react-native-elements";
import { launchCamera, launchImageLibrary } from "react-native-image-picker";
import { TextInput } from "react-native-paper";
import { commanApi } from "../../Api/api";
import { COLORS } from "../../configs/constants.config";

export default class VehicleAddScreen extends Component {
    state = {
        category_id : '1',
        reg_number  : '',
        date        : new Date(),
        documentArr : [],
        open        : false,
        categories  :[],
        documents   :[],
        modalVisible: false,
    };
    VehicleNumber = (reg_number) => {
        console.log(reg_number.toUpperCase());
        this.setState({ reg_number: reg_number.toUpperCase() });
    }
    setOpen = async (status) => {
        console.log(status);
        this.setState({ open: status });
    }
    setDate = async (date) => {
        console.log('date', date);
        this.setState({ date: date });
    }
    navigateTo = (path, params = null) => {
        let { navigation } = this.props;

        navigation.navigate(path, params);
    }

    

    CategoryApi = async () => {
        let { language } = this.state;
        let url = `category?language=${language}`
        this.setState({ loading: true });
        let response = await commanApi(url);
        if (response.status) {
            if (response.data.status) {
                this.setState({ categories: response.data.data });
                setTimeout(() => {
                    this.setState({ loading: false });
                }, 30);
            } 
        }
    }

    DocumentApi = async () => {
        let { language } = this.state;
        let url = `document?language=${language}`
        this.setState({ loading: true });
        let response = await commanApi(url);
        if (response.status) {
            if (response.data.status) {
                this.setState({ documents: response.data.data });
                setTimeout(() => {
                    this.setState({ loading: false });
                }, 30);
            } 
        }
    }

    selectCategory = category_id => {
        this.setState({ category_id });

        let documentArr = [];

        this.state.documents.forEach((doc) => {
            if(((category_id === 1 || category_id === 2) && doc.id <= 3) || (category_id !== 1 && category_id !== 2 && doc.id !== 6)) {
                documentArr.push({
                    name: doc.name,
                    document_id: doc.id,
                    expiry_date: '',
                    image: ''
                });
            }
        });

        // console.log('document arr: ', documentArr);
        this.setState({ documentArr });

    }

    handleChoosePhoto (type = 'camera', field = 'photo') {
        console.log('field',this.state.field);
        let self = this
        this.setState({modalVisible: false})
        const options = {
                noData: true,
        };
        type === 'camera' ? 
                launchCamera(options, (response) => {

                        console.log('res',response);
                        if (response.assets[0].uri) {
                                self.setState({ [this.state.field]: response.assets[0] });                                
                        }
                })
        : launchImageLibrary(options, (response) => {
                if (response.assets[0].uri) {
                        self.setState({ [this.state.field]: response.assets[0] });
                }
        });
    }

    async componentDidMount() {
        this.CategoryApi();
        this.DocumentApi();
    }

    render() {
        console.log('cat',this.state.documentArr);
        return (
            <>
                <ScrollView>
                    <View style={{ marginLeft: 15, marginRight: 15, marginBottom: 15 }}>
                        <View>
                            <Text style={{ marginTop: 10, color: '#000', fontSize: 17, fontWeight:'bold' }}>Vehicle Details</Text>
                            <View style={{ borderBottomWidth: 1, borderColor: '#ccc' }}>
                                
                                <Picker
                                    selectedValue={this.state.category_id}
                                    style={{ marginTop: 10, borderBottomWidth: 1, color: '#6f6f6f', borderColor: '#ccc', backgroundColor: 'transparent' }}
                                    onValueChange={category_id => this.selectCategory(category_id)}
                                >
                                    <Picker.Item label='Select Category' value='' />
                                    {
                                        this.state.categories.map((cat,index) => {
                                            return(
                                                <Picker.Item label={cat.name} value={cat.id} />
                                            );
                                        })
                                    }
                                    
                                </Picker>
                            </View>
                            <TextInput
                                label="Vehicle Number"
                                activeUnderlineColor={COLORS.primary}
                                maxLength={10}
                                style={{ marginTop: 10, backgroundColor: 'transparent', height: 50, fontSize: 15 }}
                                onChangeText={reg_number => this.VehicleNumber(reg_number)}
                                value={this.state.reg_number}
                            />
                            {
                                this.state.category_id ? 
                                <><Text style={{ marginTop: 10, color: '#000', fontSize: 17, fontWeight:'bold' }}>Vehicle Documents</Text></>
                                : <></>
                            }
                            {
                                this.state.documentArr.map((doc,index) => {
                                    return(
                                        <>
                                        {
                                            this.state.category_id == 1 || this.state.category_id == 2 
                                            ?
                                                doc.id != 4 && doc.id != 5 && doc.id != 6 ?
                                            <>
                                            
                                                <Text style={{ marginTop: 10, color: COLORS.primary, fontSize: 17 }}>{doc.name}</Text>
                                                <View style={{ flexDirection: 'row' }}>
                                                    <TouchableOpacity onPress={() => this.setOpen(true)} style={{ flex: .6 }}>
                                                        <TextInput
                                                            dense={true}
                                                            label="Expiry Date"
                                                            activeUnderlineColor={COLORS.primary}
                                                            style={{ backgroundColor: 'transparent', height: 50, fontSize: 15 }}
                                                            value={moment(this.state.date).format('D MMMM Y')}
                                                        />
                                                    </TouchableOpacity>
                                                    <DatePicker
                                                        modal
                                                        mode="date"
                                                        open={this.state.open}
                                                        date={this.state.date}
                                                        onConfirm={(date) => {
                                                            this.setOpen(false)
                                                            this.setDate(date)
                                                        }}
                                                        onCancel={() => {
                                                            this.setOpen(false)
                                                        }}
                                                    />
                                                    <View style={{ flex: .4, alignItems: 'center', justifyContent: 'center' }}>
                                                        <TouchableOpacity onPress={() =>this.setState({modalVisible: true, field: 'photo'})} style={{ borderStyle: 'dotted', borderWidth: 1, paddingHorizontal: 10 }}>
                                                            <Icon
                                                                name='add'
                                                                type='ionicon'
                                                                color='#000'
                                                                size={30}
                                                                style={{ alignSelf: 'center' }}
                                                            />
                                                            <Text>Add File</Text>
                                                        </TouchableOpacity>
                                                    </View>
                                                </View>
                                            </>
                                            : <>
                                            </> : 
                                            <>
                                            </>
                                        }
                                        {
                                            this.state.category_id == 3 || this.state.category_id == 4 || this.state.category_id == 5 ?
                                            doc.id != 6 ?
                                            <>
                                                <Text style={{ marginTop: 10, color: COLORS.primary, fontSize: 17 }}>{doc.name}</Text>
                                                <View style={{ flexDirection: 'row' }}>
                                                    <TouchableOpacity onPress={() => this.setOpen(true)} style={{ flex: .6 }}>
                                                        <TextInput
                                                            dense={true}
                                                            label="Expiry Date"
                                                            activeUnderlineColor={COLORS.primary}
                                                            style={{ backgroundColor: 'transparent', height: 50, fontSize: 15 }}
                                                            value={moment(this.state.date).format('D MMMM Y')}
                                                        />
                                                    </TouchableOpacity>
                                                    <DatePicker
                                                        modal
                                                        mode="date"
                                                        open={this.state.open}
                                                        date={this.state.date}
                                                        onConfirm={(date) => {
                                                            this.setOpen(false)
                                                            this.setDate(date)
                                                        }}
                                                        onCancel={() => {
                                                            this.setOpen(false)
                                                        }}
                                                    />
                                                    <View style={{ flex: .4, alignItems: 'center', justifyContent: 'center' }}>
                                                        <View style={{ borderStyle: 'dotted', borderWidth: 1, paddingHorizontal: 10 }}>
                                                            <Icon
                                                                name='add'
                                                                type='ionicon'
                                                                color='#000'
                                                                size={30}
                                                                style={{ alignSelf: 'center' }}
                                                            />
                                                            <Text>Add File</Text>
                                                        </View>
                                                    </View>
                                                </View>
                                            </>
                                            : <>
                                            </> : 
                                            <>
                                            </>
                                        }
                                            
                                        </>
                                    );
                                })
                            }

                            <Modal
                                animationType="slide"
                                transparent={true}
                                visible={this.state.modalVisible}
                                onRequestClose={() => {
                                Alert.alert("Modal has been closed.");
                                this.setState({modalVisible: false});
                                }}
                                >
                                <View style={styles.centeredView}>
                                    <View style={styles.modalView}>
                                        <TouchableOpacity onPress={()=>this.handleChoosePhoto('camera', this.state.field)}>
                                                <Text style={styles.modalText}>Take Photo...</Text>
                                        </TouchableOpacity>
                                        <TouchableOpacity onPress={()=>this.handleChoosePhoto('library')}>
                                                <Text style={styles.modalText}>Choose From library...</Text>
                                        </TouchableOpacity>
                                
                                        <TouchableOpacity
                                        onPress={() => this.setState({modalVisible: false})}
                                        >
                                            <Text style={styles.textStyle}>Cancel</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </Modal>
                            
                            {/* <Text style={{ marginTop: 10, color: COLORS.primary, fontSize: 17 }}>Permit</Text>
                            <View style={{ flexDirection: 'row' }}>
                                <TouchableOpacity onPress={() => this.setOpen(true)} style={{ flex: .6 }}>
                                    <TextInput
                                        dense={true}
                                        label="Expiry Date"
                                        activeUnderlineColor={COLORS.primary}
                                        style={{ backgroundColor: 'transparent', height: 50, fontSize: 15 }}
                                        value={moment(this.state.date).format('D MMMM Y')}
                                    />
                                </TouchableOpacity>
                                <DatePicker
                                    modal
                                    mode="date"
                                    open={this.state.open}
                                    date={this.state.date}
                                    onConfirm={(date) => {
                                        this.setOpen(false)
                                        this.setDate(date)
                                    }}
                                    onCancel={() => {
                                        this.setOpen(false)
                                    }}
                                />
                                <View style={{ flex: .4, alignItems: 'center', justifyContent: 'center' }}>
                                    <View style={{ borderStyle: 'dotted', borderWidth: 1, paddingHorizontal: 10 }}>
                                        <Icon
                                            name='add'
                                            type='ionicon'
                                            color='#000'
                                            size={30}
                                            style={{ alignSelf: 'center' }}
                                        />
                                        <Text>Add File</Text>
                                    </View>
                                </View>
                            </View> */}
                        </View>

                        {/* <View>
                            <Text style={{ marginTop: 10, color: COLORS.primary, fontWeight: 'bold', fontSize: 16 }}>Private Car</Text>
                            <View style={{ marginTop: 15, backgroundColor: COLORS.white, shadowColor: '#000', shadowOffset: { width: 0, height: 1 }, shadowOpacity: 0.8, shadowRadius: 2, elevation: 5, borderRadius: 8 }}>
                                <View style={{ flexDirection: 'row' }}>
                                    <View style={{ flex: 0.2, justifyContent: 'center', paddingTop: 10, paddingBottom: 10 }}>
                                        <Icon
                                            name='car'
                                            type='antdesign'
                                            color='#000'
                                            size={32}
                                        />
                                    </View>
                                    <View style={{ flex: 0.8, paddingTop: 10, paddingBottom: 10 }}>
                                        <Text>RJ19AB0001</Text>
                                        <Text style={{ fontSize: 11 }}>Added 1 week ago</Text>
                                    </View>
                                </View>
                            </View>
                        </View> */}
                    </View>
                </ScrollView>
            </>
        );
    }
}

export const styles = StyleSheet.create({
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 0,
        backgroundColor:'rgba(52, 52, 52, .3)',
        
      },
      modalView: {
        margin: 0,
        width:300,
        backgroundColor: "white",
        borderRadius: 5,
        padding: 35,
        // alignItems: "left",
        shadowColor: "#000",
        shadowOffset: {
          width: 0,
          height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5
      },
      button: {
        borderRadius: 20,
        padding: 10,
        elevation: 2
      },
      buttonOpen: {
        backgroundColor: "#F194FF",
      },
      buttonClose: {
        backgroundColor: "#2196F3",
      },
      textStyle: {
        color: "#000",
        fontWeight: "bold",
        textAlign: "right",
        fontSize:17,
        textTransform:'uppercase'
      },
      modalText: {
        marginBottom: 15,
        paddingVertical:8,
        fontSize:17
      },
})