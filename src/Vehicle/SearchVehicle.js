import AsyncStorage from "@react-native-async-storage/async-storage";
import moment from "moment";
import React, { Component } from "react";
import { ActivityIndicator, Pressable, ScrollView, StyleSheet, Text, TextInput, TouchableOpacity, View } from "react-native";
import { Icon } from "react-native-elements";
import { FAB } from "react-native-paper";
import { commanApi } from "../../Api/api";
import { COLORS } from "../../configs/constants.config";

export default class SearchVehicle extends Component {
    state = {
        name : '',
        loading: false,
        user: null,
        language: 'en',
        vehicles: [],
        open: false
    }

    navigateTo = (path, params = null) => {
        let { navigation } = this.props;

        navigation.navigate(path, params);
    }


    ArticleApi = async () => {
        let { language, user } = this.state;
        let url = `vehicles?language=${language}&user_id=${user.id}`
        this.setState({ loading: true });
        let response = await commanApi(url);
        console.log('res', response);
        if (response.status) {
            if (response.data.status) {
                this.setState({ vehicles: response.data.data });
                setTimeout(() => {
                    this.setState({ loading: false });
                }, 30);
            } else {
                this.setState({ loading: false });
                Alert.alert(
                    "Error",
                    response.data.message,
                    [
                        { text: "OK", onPress: () => console.log("OK Pressed") }
                    ]
                );
            }
        }
    }
    async componentDidMount() {
        let user = await AsyncStorage.getItem('@user');
        if (user) {
            user = JSON.parse(user);
            console.log('user details', user);
            this.setState({ user: user });
        }
        this.ArticleApi();
    }

    render() {
        console.log('vehicle', this.state.vehicles);
        return (
            <>
                {
                    this.state.loading ?
                        <ActivityIndicator size="large" color={COLORS.primary} style={{ marginTop: 30 }} />
                        :
                        <>
                            <ScrollView>
                                <View style={{ marginLeft: 15, marginRight: 15, marginBottom: 15 }}>
                                <View >
                                    <TextInput style={styles.input}
                                        underlineColorAndroid = "transparent"
                                        placeholder = "Search"
                                        placeholderTextColor = "#aaa"
                                        autoCapitalize = "none"
                                        value={this.state.name}  
                                        onChangeText={name => this.setState({ name })}
                                        
                                    />
                                    
                                    <TouchableOpacity onPress={() => this.state.name != '' ? this.setState({name:''}) : ''}  style={{position:'absolute',right:10,bottom:'17%'}}>
                                        <Icon color='#a1a1a1' size={20} type='antdesign'  name={this.state.name == '' ? 'search1' : 'close'} />
                                    </TouchableOpacity>
                                </View>
                                    {
                                        this.state.vehicles.category && this.state.vehicles.category.map((item, index) => {
                                            return (
                                                <View>
                                                    <Text style={{ marginTop: 10, color: COLORS.primary, fontWeight: 'bold', fontSize: 16 }}>{item.name}</Text>
                                                    {
                                                        item.vehicle.map((v, index) => {
                                                            return (
                                                                <View style={{ marginTop: 15, backgroundColor: COLORS.white, shadowColor: '#000', shadowOffset: { width: 0, height: 1 }, shadowOpacity: 0.8, shadowRadius: 2, elevation: 5, borderRadius: 8 }}>
                                                                    <TouchableOpacity style={{ flexDirection: 'row' }} onPress={() => this.navigateTo('SingleVehicle', v)}>
                                                                        <View style={{ flex: 0.2, justifyContent: 'center', paddingTop: 10, paddingBottom: 10 }}>
                                                                            {(() => {
                                                                                if (v.category_id == 1) {
                                                                                    return (
                                                                                        <Icon
                                                                                            name='car'
                                                                                            type='antdesign'
                                                                                            color='#000'
                                                                                            size={32}
                                                                                        />
                                                                                    )
                                                                                }
                                                                                return null;
                                                                            })()}
                                                                            {(() => {
                                                                                if (v.category_id == 2) {
                                                                                    return (
                                                                                        <Icon
                                                                                            name='motorcycle'
                                                                                            type='materialicon'
                                                                                            color='#000'
                                                                                            size={32}
                                                                                        />
                                                                                    )
                                                                                }
                                                                                return null;
                                                                            })()}
                                                                            {(() => {
                                                                                if (v.category_id == 3) {
                                                                                    return (
                                                                                        <Icon
                                                                                            name='truck'
                                                                                            type='feather'
                                                                                            color='#000'
                                                                                            size={32}
                                                                                        />
                                                                                    )
                                                                                }
                                                                                return null;
                                                                            })()}
                                                                            {(() => {
                                                                                if (v.category_id == 4) {
                                                                                    return (
                                                                                        <Icon
                                                                                            name='bus-outline'
                                                                                            type='ionicon'
                                                                                            color='#000'
                                                                                            size={32}
                                                                                        />
                                                                                    )
                                                                                }
                                                                                return null;
                                                                            })()}
                                                                            {(() => {
                                                                                if (v.category_id == 5) {
                                                                                    return (
                                                                                        <Icon
                                                                                            name='car'
                                                                                            type='antdesign'
                                                                                            color='#000'
                                                                                            size={32}
                                                                                        />
                                                                                    )
                                                                                }
                                                                                return null;
                                                                            })()}

                                                                        </View>
                                                                        <View style={{ flex: 0.8, paddingTop: 10, paddingBottom: 10 }}>
                                                                            <Text style={{ textTransform: 'uppercase' }}>{v.reg_number}{v.id}</Text>
                                                                            <Text style={{ fontSize: 11 }}>Added {moment.utc(v.created_at).local().startOf('seconds').fromNow()}</Text>
                                                                        </View>
                                                                    </TouchableOpacity>
                                                                </View>
                                                            )
                                                        })
                                                    }

                                                </View>
                                            )
                                        })
                                    }
                                </View>
                            </ScrollView>
                        </>
                }
                
            </>
        );
    }
}
const styles = StyleSheet.create({
    input:{
        backgroundColor:'#fff',
        // height:25,
        paddingVertical:5,
        borderRadius:8,
        marginTop:10
        // marginLeft:-30
    }
})