import { Picker } from "@react-native-picker/picker";
import moment from "moment";
import React, { Component } from "react";
import { Alert, AsyncStorage, Button, Image, Modal, Pressable, ScrollView, StyleSheet, Text, TouchableOpacity, View } from "react-native";
// import DatePicker from "react-native-date-picker";
import DatePicker from 'react-native-datepicker';
import { Icon } from "react-native-elements";
import { launchCamera, launchImageLibrary } from "react-native-image-picker";
import { TextInput } from "react-native-paper";
import { addVehicle, commanApi } from "../../Api/api";
import { COLORS } from "../../configs/constants.config";

export default class VehicleAddScreen extends Component {
    state = {
        user: null,
        category_id: null,
        reg_number: '',
        date: new Date(),
        documentArr: [],
        open: false,
        categories: [],
        documents: [],
        modalVisible: false,
        selectedIndex: null
    };
    VehicleNumber = (reg_number) => {
        console.log(reg_number.toUpperCase());
        this.setState({ reg_number: reg_number.toUpperCase() });
    }
    setOpen = async (status) => {
        console.log(status);
        this.setState({ open: status });
    }
    setDate = async (date) => {
        console.log('date', date);
        this.setState({ date: date });
    }
    navigateTo = (path, params = null) => {
        let { navigation } = this.props;

        navigation.navigate(path, params);
    }

    CategoryApi = async () => {
        let { language } = this.state;
        let url = `category?language=${language}`
        this.setState({ loading: true });
        let response = await commanApi(url);
        if (response.status) {
            if (response.data.status) {
                this.setState({ categories: response.data.data });
                setTimeout(() => {
                    this.setState({ loading: false });
                }, 30);
            }
        }
    }

    DocumentApi = async () => {
        let { language } = this.state;
        let url = `document?language=${language}`
        this.setState({ loading: true });
        let response = await commanApi(url);
        if (response.status) {
            if (response.data.status) {
                this.setState({ documents: response.data.data });
                setTimeout(() => {
                    this.setState({ loading: false });
                }, 30);
            }
        }
    }

    selectCategory = category_id => {
        this.setState({ category_id });

        let documentArr = [];

        this.state.documents.forEach((doc) => {
            if (((category_id === 1 || category_id === 2) && doc.id <= 3) || (category_id !== 1 && category_id !== 2 && doc.id !== 6)) {
                documentArr.push({
                    name: doc.name,
                    document_id: doc.id,
                    expiry_date: moment.utc(new Date()).format('YYYY-MM-DD'),
                    image: ''
                });
            }
        });

        // console.log('document arr: ', documentArr);
        this.setState({ documentArr });

    }

    handleChoosePhoto(type = 'camera', field = 'photo') {
        console.log('field', this.state.field);
        let self = this
        this.setState({ modalVisible: false })
        const options = {
            noData: true,
        };
        type === 'camera' ?
            launchCamera(options, (response) => {

                console.log('res', response);
                if (response.assets && response.assets[0].uri) {
                    // self.setState({ [this.state.field]: response.assets[0] });       
                    self.updateDocFeild('image', self.state.selectedIndex, response.assets[0]);
                }
            })
            : launchImageLibrary(options, (response) => {
                // if (response.assets[0].uri) {
                //         self.setState({ [this.state.field]: response.assets[0] });
                // }
                if (response.assets && response.assets[0].uri) {
                    // self.setState({ [this.state.field]: response.assets[0] });       
                    self.updateDocFeild('image', self.state.selectedIndex, response.assets[0]);
                }
            });
    }

    updateDocFeild = (field, index, value) => {
        let documentArr = this.state.documentArr;

        documentArr[index][field] = value;

        this.setState({ documentArr });
        console.log('doc Arr: ', this.state.documentArr);


    }
    updateDocDate = async (value, index) => {
        let documentArr = this.state.documentArr;

        documentArr[index]['expiry_date'] = moment.utc(new Date()).format('YYYY-MM-DD');

        this.setState({ documentArr });
        console.log('doc Arr: ', this.state.documentArr);
    }

    saveVehicle = async () => {
        let { category_id, reg_number, documentArr, user } = this.state;

        this.setState({ loading: true });
        let data = {
            reg_number: reg_number,
            category_id: category_id,
            user_id: user.id,
            document: documentArr
        }

        let response = await addVehicle(data);
        console.log('data', response);
        if (response.status) {
            if (response.data.status) {
                setTimeout(() => {
                    this.navigateTo('Vehicle', response.data.data);
                }, 30);
            } else {
                this.setState({ loading: false });
                Alert.alert(
                    "Error",
                    response.data.response_message,
                    [
                        { text: "OK", onPress: () => console.log("OK Pressed") }
                    ]
                );
            }
        }
    }
    createFormData = (documentArr, body = {}) => {
        const data = new FormData();
        documentArr.forEach(doc => {
            if (doc.image) {
                data.append('image', {
                    name: doc.image.fileName,
                    type: doc.image.type,
                    uri: Platform.OS === 'ios' ? doc.image.uri.replace('file://', '') : doc.image.uri,
                });
            }
        });
        Object.keys(body).forEach((key) => {
            data.append(key, body[key]);
        });

        return data;
    }

    async componentDidMount() {
        let user = await AsyncStorage.getItem('@user');
        if (user) {
            user = JSON.parse(user);
            console.log('user details', user);
            this.setState({ user: user });
        }
        this.CategoryApi();
        this.DocumentApi();
    }

    render() {
        // console.log('cat',this.state.documentArr);
        // console.log('doc Arr: ', this.state.documentArr);
        return (
            <>
                <ScrollView>
                    <View style={{ marginLeft: 15, marginRight: 15, marginBottom: 15 }}>
                        <View>
                            <Text style={{ marginTop: 10, color: '#000', fontSize: 17, fontWeight: 'bold' }}>Vehicle Details</Text>
                            <View style={{ borderBottomWidth: 1, borderColor: '#ccc' }}>

                                <Picker
                                    selectedValue={this.state.category_id}
                                    style={{ marginTop: 10, borderBottomWidth: 1, color: '#6f6f6f', borderColor: '#ccc', backgroundColor: 'transparent' }}
                                    onValueChange={category_id => this.selectCategory(category_id)}
                                >
                                    <Picker.Item label='Select Category' value='' />
                                    {
                                        this.state.categories.map((cat, index) => {
                                            return (
                                                <Picker.Item label={cat.name} value={cat.id} />
                                            );
                                        })
                                    }

                                </Picker>
                            </View>
                            <TextInput
                                label="Vehicle Number"
                                activeUnderlineColor={COLORS.primary}
                                maxLength={10}
                                style={{ marginTop: 10, backgroundColor: 'transparent', height: 50, fontSize: 15 }}
                                onChangeText={reg_number => this.VehicleNumber(reg_number)}
                                value={this.state.reg_number}
                            />
                            {
                                this.state.category_id ?
                                    <><Text style={{ marginTop: 10, color: '#000', fontSize: 17, fontWeight: 'bold' }}>Vehicle Documents</Text></>
                                    : <></>
                            }
                            {
                                this.state.documentArr.map((doc, index) => {
                                    console.log('doc: ', doc.expiry_date);
                                    return (
                                        <>

                                            <Text style={{ marginTop: 10, color: COLORS.primary, fontSize: 17 }}>{doc.name}</Text>
                                            <View style={{ flexDirection: 'row' }}>
                                                {/* <TouchableOpacity onPress={() => this.setOpen(true)} style={{ flex: .6 }}>
                                                        <TextInput
                                                            dense={true}
                                                            label="Expiry Date"
                                                            activeUnderlineColor={COLORS.primary}
                                                            style={{ backgroundColor: 'transparent', height: 50, fontSize: 15 }}
                                                            value={moment(doc.expiry_date).format('D MMMM Y')}
                                                        />
                                                    </TouchableOpacity>
                                                    <DatePicker
                                                        modal
                                                        mode="date"
                                                        open={this.state.open}
                                                        date={doc.expiry_date}
                                                        onConfirm={(date) => {
                                                            this.setOpen(false)
                                                            this.setDate(date)

                                                            this.updateDocFeild('expiry_date', index, date);
                                                        }}
                                                        onCancel={() => {
                                                            this.setOpen(false)
                                                        }}
                                                    /> */}
                                                <View style={{ flex: .6 }}>
                                                    {/* <Text>{doc.image}</Text> */}
                                                    <DatePicker
                                                        style={{ width: '100%', marginVertical: 10, borderRadius: 5, backgroundColor: '#fff', }}
                                                        date={doc.expiry_date} // Initial date from state
                                                        value={doc.expiry_date}
                                                        mode="date" // The enum of date, datetime and time
                                                        placeholder="select date"
                                                        format="YYYY-MM-DD"
                                                        // minDate="01-01-2016"
                                                        // maxDate="01-01-2019"
                                                        confirmBtnText="Confirm"
                                                        cancelBtnText="Cancel"
                                                        customStyles={{
                                                            dateIcon: {
                                                                //display: 'none',
                                                                position: 'absolute',
                                                                right: 0,
                                                                top: 4,
                                                                marginRight: 0,
                                                            },
                                                            dateInput: {
                                                                marginRight: 36,
                                                                borderRadius: 5,
                                                                borderColor: '#00afef'
                                                            },
                                                        }}
                                                        onDateChange={date => this.updateDocDate(date, index)}
                                                    />
                                                </View>
                                                <View style={{ flex: .4, alignItems: 'center', justifyContent: 'center' }}>
                                                    <TouchableOpacity onPress={() => this.setState({ modalVisible: true, field: 'photo', selectedIndex: index })} style={{ borderStyle: 'dotted', borderWidth: 1, paddingHorizontal: 10 }}>
                                                        {
                                                            doc.image ?
                                                                <Image source={doc.image} style={{ width: 50, height: 50, resizeMode: 'cover' }} />
                                                                :
                                                                <>
                                                                    <Icon
                                                                        name='add'
                                                                        type='ionicon'
                                                                        color='#000'
                                                                        size={30}
                                                                        style={{ alignSelf: 'center' }}
                                                                    />
                                                                    <Text>Add File</Text>
                                                                </>
                                                        }
                                                    </TouchableOpacity>
                                                </View>
                                            </View>
                                        </>
                                    );
                                })
                            }
                            {
                                this.state.category_id && this.state.reg_number ?
                                    <Button onPress={() => this.saveVehicle()} title="ADD" color={COLORS.primary}></Button>
                                    :
                                    <Text title="Update" style={{ textAlign: 'center', backgroundColor: COLORS.primary_fade, paddingVertical: 7, color: COLORS.white, fontWeight: 'bold' }} >ADD</Text>
                            }

                            <Modal
                                animationType="slide"
                                transparent={true}
                                visible={this.state.modalVisible}
                                onRequestClose={() => {
                                    Alert.alert("Modal has been closed.");
                                    this.setState({ modalVisible: false });
                                }}
                            >
                                <View style={styles.centeredView}>
                                    <View style={styles.modalView}>
                                        <TouchableOpacity onPress={() => this.handleChoosePhoto('camera', this.state.field)}>
                                            <Text style={styles.modalText}>Take Photo...</Text>
                                        </TouchableOpacity>
                                        <TouchableOpacity onPress={() => this.handleChoosePhoto('library')}>
                                            <Text style={styles.modalText}>Choose From library...</Text>
                                        </TouchableOpacity>

                                        <TouchableOpacity
                                            onPress={() => this.setState({ modalVisible: false })}
                                        >
                                            <Text style={styles.textStyle}>Cancel</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </Modal>

                            {/* <Text style={{ marginTop: 10, color: COLORS.primary, fontSize: 17 }}>Permit</Text>
                            <View style={{ flexDirection: 'row' }}>
                                <TouchableOpacity onPress={() => this.setOpen(true)} style={{ flex: .6 }}>
                                    <TextInput
                                        dense={true}
                                        label="Expiry Date"
                                        activeUnderlineColor={COLORS.primary}
                                        style={{ backgroundColor: 'transparent', height: 50, fontSize: 15 }}
                                        value={moment(this.state.date).format('D MMMM Y')}
                                    />
                                </TouchableOpacity>
                                <DatePicker
                                    modal
                                    mode="date"
                                    open={this.state.open}
                                    date={this.state.date}
                                    onConfirm={(date) => {
                                        this.setOpen(false)
                                        this.setDate(date)
                                    }}
                                    onCancel={() => {
                                        this.setOpen(false)
                                    }}
                                />
                                <View style={{ flex: .4, alignItems: 'center', justifyContent: 'center' }}>
                                    <View style={{ borderStyle: 'dotted', borderWidth: 1, paddingHorizontal: 10 }}>
                                        <Icon
                                            name='add'
                                            type='ionicon'
                                            color='#000'
                                            size={30}
                                            style={{ alignSelf: 'center' }}
                                        />
                                        <Text>Add File</Text>
                                    </View>
                                </View>
                            </View> */}
                        </View>

                        {/* <View>
                            <Text style={{ marginTop: 10, color: COLORS.primary, fontWeight: 'bold', fontSize: 16 }}>Private Car</Text>
                            <View style={{ marginTop: 15, backgroundColor: COLORS.white, shadowColor: '#000', shadowOffset: { width: 0, height: 1 }, shadowOpacity: 0.8, shadowRadius: 2, elevation: 5, borderRadius: 8 }}>
                                <View style={{ flexDirection: 'row' }}>
                                    <View style={{ flex: 0.2, justifyContent: 'center', paddingTop: 10, paddingBottom: 10 }}>
                                        <Icon
                                            name='car'
                                            type='antdesign'
                                            color='#000'
                                            size={32}
                                        />
                                    </View>
                                    <View style={{ flex: 0.8, paddingTop: 10, paddingBottom: 10 }}>
                                        <Text>RJ19AB0001</Text>
                                        <Text style={{ fontSize: 11 }}>Added 1 week ago</Text>
                                    </View>
                                </View>
                            </View>
                        </View> */}
                    </View>
                </ScrollView>
            </>
        );
    }
}

export const styles = StyleSheet.create({
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 0,
        backgroundColor: 'rgba(52, 52, 52, .3)',

    },
    modalView: {
        margin: 0,
        width: 300,
        backgroundColor: "white",
        borderRadius: 5,
        padding: 35,
        // alignItems: "left",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5
    },
    button: {
        borderRadius: 20,
        padding: 10,
        elevation: 2
    },
    buttonOpen: {
        backgroundColor: "#F194FF",
    },
    buttonClose: {
        backgroundColor: "#2196F3",
    },
    textStyle: {
        color: "#000",
        fontWeight: "bold",
        textAlign: "right",
        fontSize: 17,
        textTransform: 'uppercase'
    },
    modalText: {
        marginBottom: 15,
        paddingVertical: 8,
        fontSize: 17
    },
})