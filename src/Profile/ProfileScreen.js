import AsyncStorage from "@react-native-async-storage/async-storage";
import React, { Component } from "react";
import { Button, ScrollView, Text, View } from "react-native";
import { TextInput } from "react-native-paper";
import { commanApi, editProfile } from "../../Api/api";
import { COLORS } from "../../configs/constants.config";

export default class ProfileScreen extends Component {
    state = {
        fname   : '',
        lname   : '',
        email   : '',
        mobile  : '',
        user    : null,
        profile : '',
        language: 'en',
    };
    firstName = (fname) => {
        this.setState({ fname: fname });
    }
    lastName = (lname) => {
        this.setState({ lname: lname });
    }
    Email = (email) => {
        this.setState({ email: email });
    }
    navigateTo = (path, params = null) => {
        let { navigation } = this.props;

        navigation.navigate(path, params);
    }

    ProfileApi = async () => {
        let { user } = this.state;
        this.setState({ loading: true });
        let response = await commanApi(`profile/${user.id}`);
        if (response.status) {
            if (response.data.status) {
                this.setState({ profile: response.data.data });
                setTimeout(() => {
                    this.setState({ loading: false });
                }, 30);
                this.setState({fname:this.state.profile.first_name});
                this.setState({lname:this.state.profile.last_name});
                this.setState({email:this.state.profile.email});
            } else {
                this.setState({ loading: false });
                Alert.alert(
                    "Error",
                    response.data.message,
                    [
                        { text: "OK", onPress: () => console.log("OK Pressed") }
                    ]
                );
            }
        }
    }


    EditProfileApi = async () => {
        let { mobile, fname, lname, email, user } = this.state;
        
        this.setState({ loading: true });
        let data = {
            first_name  : fname,
            last_name   : lname,
            email       : email,
            user_id     : user.id
        }
        let response = await editProfile(data);
        if (response.status) {
            if (response.data.status) {
                console.log('response1', response.data);
                // await AsyncStorage.setItem('@user', JSON.stringify(response.data.data));
                setTimeout(() => {
                    // this.navigateTo('Verify', response.data.data);
                    this.setState({ loading: false });
                    this.setState({ mobile: '' });
                }, 30);
            } else {
                this.setState({ loading: false });
                Alert.alert(
                    "Error",
                    response.data.response_message,
                    [
                        { text: "OK", onPress: () => console.log("OK Pressed") }
                    ]
                );
            }
        }
        
    }
    async componentDidMount() {
        let user = await AsyncStorage.getItem('@user');
        if (user) {
            user = JSON.parse(user);
            console.log('user details', user);
            this.setState({ user: user });
        }
        this.ProfileApi();
        
    }

    render() {
        console.log('profile',this.state.profile.first_name);
        return (
            <>
                <ScrollView>
                    <View style={{ marginLeft: 15, marginRight: 15, marginBottom: 15 }}>
                        <View>
                            <TextInput
                                label="First Name"
                                activeUnderlineColor={COLORS.primary}
                                style={{ marginTop: 10, backgroundColor: 'transparent', height: 50, fontSize: 15 }}
                                onChangeText={fname => this.firstName(fname)}
                                value={this.state.fname}
                            />
                            <TextInput
                                label="Last Name"
                                activeUnderlineColor={COLORS.primary}
                                style={{ marginTop: 10, backgroundColor: 'transparent', height: 50, fontSize: 15 }}
                                onChangeText={lname => this.lastName(lname)}
                                value={this.state.lname}
                            />
                            <TextInput
                                label="Email"
                                activeUnderlineColor={COLORS.primary}
                                style={{ marginTop: 10, marginBottom: 20, backgroundColor: 'transparent', height: 50, fontSize: 15 }}
                                onChangeText={email => this.Email(email)}
                                value={this.state.email}
                            />
                            {
                                this.state.fname && this.state.lname && this.state.email ?
                                <Button onPress={() => this.EditProfileApi()} title="Update" color={COLORS.primary}></Button>
                                :
                                <Text title="Update" style={{textAlign:'center',backgroundColor:COLORS.primary_fade, paddingVertical:7, color:COLORS.white, fontWeight:'bold'}} >UPDATE</Text>
                            }
                        </View>
                    </View>
                </ScrollView>
            </>
        );
    }
}